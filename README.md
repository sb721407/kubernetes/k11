# SkillBox DevOps. Kubernetes. Часть 11. Kubernetes для непрерывной поставки. Интеграция с CI-сервисом

SkillBox DevOps. Kubernetes. Часть 11. Kubernetes для непрерывной поставки. Интеграция с CI-сервисом

--------------------------------------------------------

## Задание 1. Настройка liveness проверки приложения

https://gitlab.com/sb721407/kubernetes/k11/-/blob/main/Homework/task-1/app-1.yaml

Скриншоты с результатами выполнения Задания 1:

https://gitlab.com/sb721407/kubernetes/k11/-/blob/main/Homework/png/task-1_01.png

https://gitlab.com/sb721407/kubernetes/k11/-/blob/main/Homework/png/task-1_02.png

## Задание 2. Настройка readiness проверки приложения

https://gitlab.com/sb721407/kubernetes/k11/-/blob/main/Homework/task-2/app-2.yaml

Скриншот с результатами выполнения Задания 2:

https://gitlab.com/sb721407/kubernetes/k11/-/blob/main/Homework/png/task-2.png

